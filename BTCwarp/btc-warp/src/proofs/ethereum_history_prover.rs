use crate::proofs::generators::{generate_ethereum_proof, EthereumProof};
use ethereum_types::H256;
use std::collections::HashMap;

pub struct EthereumHistoryProver {
    pub headers: HashMap<H256, ethereum_types::Header>,
    pub proof: Option<EthereumProof>,
}

impl EthereumHistoryProver {
    pub fn new() -> Self {
        EthereumHistoryProver {
            headers: HashMap::new(),
            proof: None,
        }
    }

    pub fn add_header(&mut self, header: ethereum_types::Header) {
        self.headers.insert(header.hash(), header);
    }

    pub fn generate_proof(&mut self, target_header_hash: H256) -> Result<(), String> {
        let proof = generate_ethereum_proof(&self.headers, target_header_hash)?;
        self.proof = Some(proof);
        Ok(())
    }

    pub fn get_proof(&self) -> Option<&EthereumProof> {
        self.proof.as_ref()
    }
}