package verifier

import (
	"github.com/btcsuite/btcd/btcec"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/succinctlabs/gnark-plonky2-verifier/field"
	"github.com/succinctlabs/gnark-plonky2-verifier/verifier/common"
	"github.com/succinctlabs/gnark-plonky2-verifier/verifier/internal/plonk"
)

type Proof struct {
	WiresCap                  common.MerkleCap
	PlonkZsPartialProductsCap common.MerkleCap
	QuotientPolysCap          common.MerkleCap
	Openings                  plonk.OpeningSet
	OpeningProof              common.FriProof
}

type ProofWithPublicInputs struct {
	Proof        Proof
	PublicInputs []field.F
}

type ProofChallenges struct {
	PlonkBetas    []field.F
	PlonkGammas   []field.F
	PlonkAlphas   []field.F
	PlonkZeta     field.QuadraticExtension
	FriChallenges common.FriChallenges
}

// Ethereum-specific data structures
type EthereumData struct {
	EthAddress    common.Address
	EthPrivateKey *crypto.PrivateKey
	EthPublicKey  *crypto.PublicKey
}

// Bitcoin-specific data structures
type BitcoinData struct {
	BtcPrivateKey *btcec.PrivateKey
	BtcPublicKey  *btcec.PublicKey
}
